local screens = {
  {
    output = "DVI-D-O",
    resolution = {
      mode = "1920x1080",
      scale = 1,
      rate = 120,
    },
    pos = "1920x0",
  },
  {
    output = "HDMI-0",
    resolution = {
      mode = "3840x2160",
      scale = 0.5,
      rate = 60,
    },
    pos = "3840x0",
  },
  {
    output = "DP-4",
    resolution = {
      mode = "1920x1080",
      scale = 1,
      rate = 60,
    },
    pos = "0x0",
  },
}

local xrandr = ""

for current,i in pairs(screens) do -- always keep at least one space in the start and end of each string
  xrandr = xrandr .. " --output " .. i.output
  xrandr = xrandr .. " --mode "   .. i.resolution.mode
  xrandr = xrandr .. " --scale "  .. i.resolution.scale
  xrandr = xrandr .. " --rate "   .. i.resolution.rate
  xrandr = xrandr .. " --pos "    .. i.pos
  print(xrandr)
  if current == 1 then xrandr = xrandr .. " --primary " end
end

print("xrandr" .. xrandr)
os.execute("xrandr" .. xrandr)
